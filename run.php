<?php
define('ROOT', __DIR__);
require_once 'lib/Bot.php';
ini_set('display_errors', 1);

$config = json_decode(file_get_contents('config.json'), true);

$bot = new Bot($config);
$bot->run();
