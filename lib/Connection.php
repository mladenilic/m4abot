<?php

class Connection
{
    protected $_socket;
    protected $_server;
    protected $_port;
    
    public function __construct($server, $port)
    {
        $this->_server = $server;
        $this->_port = $port;
    }
    
    public function connect()
    {
        $this->_socket = fsockopen($this->_server, $this->_port, $asd, $sfas);
    }
    
    public function disconnect()
    {
        fclose($this->_socket);
    }
    
    public function isConnected() 
    {
        return is_resource($this->_socket);
    }
    
    public function sendData($data) 
    {
        return fwrite($this->_socket, $data . "\r\n");
    }
    
    public function getData()
    {
        if (!$this->isConnected()) {
            throw new Exception('Cannot get data. Socket closed');
        }
        return fgets($this->_socket);
    }
}