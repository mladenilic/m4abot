<?php

class Db
{
  protected $_dbConnection;
  protected $_charactersDb;
  protected $_realmDb;
  protected $_worldDb;
  
  public function __construct($config)
  {          
      $this->_dbConnection = new PDO("mysql:host={$config['host']};charset=utf8", $config['user'], $config['password']);
      $this->_dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $this->_dbConnection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
      
      $this->_charactersDb = $config['characters-db'];
      $this->_realmDb = $config['realm-db'];
      $this->_worldDb = $config['world-db'];
  } 
  
  function __destruct() 
  {
      $this->_dbConnection = null;
  }
  
  public function setDbConnection($con) 
  {
      $this->_dbConnection = $con;
    
      return $this;
  }
  
  public function getDbConnection() 
  {
      return $this->_dbConnection;
  }  
}
