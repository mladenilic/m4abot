<?php

class Parser
{    
    const REGEX = '/^(:(?<prefix>\S+) )?(?<command>\S+)( (?!:)(?<params>.+?))?( :(?<trail>.+))?$/';
    
    public function parse($data) 
    {
        if (!preg_match(self::REGEX, $data, $matches)) {
            throw new Exception('Malformend message received');
        }
        
        return array(
            'prefix'  => isset($matches['prefix']) ? $matches['prefix'] : '',
            'command' => isset($matches['command']) ? $matches['command'] : '',
            'params'  => explode(' ', isset($matches['params']) ? $matches['params'] : ''),
            'trail'   => isset($matches['trail']) ? $matches['trail'] : '',
        );
    }
}