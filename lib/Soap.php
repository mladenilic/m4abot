<?php

class Soap
{
    protected $_client;
    protected $_error;
    
    public function __construct($config)
    {
        $this->_client = new SoapClient(null, $config);
    }
    
    public function execCommand($command)
    {
        $result = "";
        try {
            $result = $this->_client->executeCommand(new SoapParam("$command", "command"));
        } catch (Exception $e) { 
            $this->setError($e->getMessage());
        }
        
        return $result;
    }
    
    public function setError($error) 
    {
        $this->_error = $error;
    }
    
    public function getError() 
    {
        return $this->_error;
    }
    
    public function hasError() 
    {
        return !empty($this->_error);
    }
}