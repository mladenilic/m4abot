<?php
require_once "Connection.php";
require_once "Parser.php";
require_once "Log.php";
require_once "Soap.php";
require_once "Ticket.php";

class Bot
{
    protected $_config;
    protected $_connection;
    protected $_parser;
    protected $_callbacks;
    
    public function __construct($config)
    {
        $this->_config = $config;
        $this->_connection = new Connection($config['server'], $config['port']);
        $this->_parser = new Parser();
        $this->_log = new Log($config['log']);
        
        $this->_handlers = array(
            '001' => function ($data) 
            {
                $this->_joinChannels();
            },
            'PING' => function ($data) 
            {
                $this->_connection->sendData("PONG {$data['params'][0]}");
            },
            'PRIVMSG' => function ($data) 
            {
                if (!preg_match('/^m4aBot::/', $data['trail'])) {
                    return false;
                }
                
                $command = trim(str_replace("m4aBot::", "", strtok($data['trail'], " ")));
                $chatCommands = array(
                    'website-update' => function() use ($data) 
                    {
                        $this->_sendResponse($data['params'][0], 'Running website update...');
                        shell_exec('cd C:\inetpub\wwwroot && git reset --hard HEAD && git pull');
                        $this->_sendResponse($data['params'][0], 'Website update completed.');
                    },
                    'invite' => function () 
                    {
                        $channelName = trim(strtok(" "));
                        $channelPass = trim(strtok(" "));
                        $this->_connection->sendData("JOIN {$channelName} {$channelPass}");
                    },
                    'leave' => function () use ($data) 
                    {
                        $this->_connection->sendData("PART {$data['params'][0]}");
                    },
                    'info' => function () use ($data) 
                    {
                        $this->_sendResponse($data['params'][0], $this->_config['info']);
                    },
                    'announce' => function () use ($data) 
                    {
                        $message = end(explode(' ', $data['trail'], 2));
                        if (empty($message)) {
                            $this->_sendResponse($data['params'][0], 'Enter announce message');
                            return false;
                        }
                        $soap = new Soap($this->_config['soap']);
                        $soap->execCommand("announce {$message}");
                        
                        if ($soap->hasError()) {
                            $this->_sendResponse($data['params'][0], $soap->getError());
                        } else {
                            $this->_sendResponse($data['params'][0], "Announced: {$message}");
                        }
                    },
                    'tickets' => function() use ($data)
                    {
                        $ticketHandler = new Ticket($this->_config['db']);
                        $tickets = $ticketHandler->getAllTickets();
                        
                        if (empty($tickets)) {
                            return;
                        }
                        
                        $this->_sendResponse($data['params'][0], "Listing all open tickets");
                        $this->_sendResponse(
                            $data['params'][0], 
                            str_pad("id", 10) . "| " . str_pad("name", 15, " ", STR_PAD_BOTH) . "| " . str_pad("preview", 50, " ", STR_PAD_BOTH) . "| " .str_pad("last changed", 15, " ", STR_PAD_BOTH)
                        );
                        $this->_sendResponse($data['params'][0], str_repeat("-", 100));
                        
                        foreach ($tickets as $ticket) {
                            $ticketText = strlen($ticket['ticket_text']) > 40 ? (substr($ticket['ticket_text'], 0, 37) . "...") : $ticket['ticket_text'];
                            $this->_sendResponse(
                                $data['params'][0], 
                                str_pad($ticket['ticket_id'], 10) . "| " . str_pad($ticket['name'], 15, " ", STR_PAD_BOTH) . "| " . str_pad(str_replace("\n", "", $ticketText), 50, " ", STR_PAD_BOTH) . "| " . str_pad($ticket['ticket_lastchange'], 15, " ", STR_PAD_BOTH)
                            ); 
                        }
                    },
                    'ticket-read' => function()  use ($data)
                    {
                        $message = trim(end(explode(' ', $data['trail'], 2)));
                        if (empty($message)) {
                            $this->_sendResponse($data['params'][0], 'Enter character name or ticket id');
                            return false;
                        }
                        $soap = new Soap($this->_config['soap']);
                        $ticket = $soap->execCommand("ticket {$message}");
                        if ($soap->hasError()) {
                            $this->_sendResponse($data['params'][0], $soap->getError());
                            return false;
                        }
                        
                        foreach (explode("\n", $ticket) as $line) {
                            $this->_sendResponse($data['params'][0], $line);
                        }
                        
                        return true;
                    },
                    'ticket-close' => function()  use ($data)
                    {
                        $message = trim(end(explode(' ', $data['trail'], 2)));
                        if (empty($message)) {
                            $this->_sendResponse($data['params'][0], 'Enter character name or ticket id');
                            return false;
                        }
                        $soap = new Soap($this->_config['soap']);
                        $ticket = $soap->execCommand("delticket {$message}");
                        $this->_sendResponse($data['params'][0], $soap->hasError() ? $soap->getError() : str_replace("\n", "", $ticket));
                    }
                );
                
                if (isset($chatCommands[$command])) {
                    $chatCommands[$command]();
                }
            }
        );
    }
    
    public function __destruct()
    {
        $this->_connection->disconnect();
    }
    
    public function run()
    {
        $this->connect();
        $this->_register();
        
        while (true) {
            $data = $this->getData();
            
            if (!$data) {
                usleep(1000000);
                continue;
            }
             
            $this->processData($data);
        }
    }
    
    public function connect()
    {
        $this->_connection->connect();
        
        return $this;
    }
    
    public function getData()
    {
        $data = $this->_connection->getData();
        
        if (!$data) {
            return false;
        }
        
        $this->_log->log($data);
        $data = $this->_parser->parse($data);
        
        return $data;
    }
    
    public function processData($data)
    {
        if (!isset($this->_handlers[$data['command']])) {
            return false;
        }
        
        $this->_handlers[$data['command']]($data);
        
        return true;
    }
    
    protected function _register()
    {
        $this->_connection->sendData("USER {$this->_config['user']} 0 * :{$this->_config['full-name']}");
        $this->_connection->sendData("NICK {$this->_config['nick']}");
        
        return $this;
    }
    
    protected function _joinChannels()
    {
        if (!isset($this->_config) || !isset($this->_config['channels'])) {
            return;
        }
        
        foreach ($this->_config['channels'] as $channel) {
            $this->_connection->sendData("JOIN {$channel['name']} {$channel['key']}");
        }
        
        return $this;
    }
    
    protected function _sendResponse($channel, $message)
    {
        $this->_connection->sendData("PRIVMSG {$channel} :{$message}");
    }
}