<?php
require_once "Db.php";

class Ticket extends Db
{
    public function getAllTickets()
    {
        $query = $this->getDbConnection()->prepare("SELECT * FROM tbc_characters.character_ticket as ticket JOIN tbc_characters.characters as characters ON ticket.guid = characters.guid");
        $query->execute();
        return $query->fetchAll();
    }
}