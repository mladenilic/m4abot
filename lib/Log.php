<?php

class Log
{
    protected $_path;
    protected $_enabled = false;
    
    public function __construct($config)
    {
        if (!empty($config['path'])) {
             $this->_path = ROOT . DIRECTORY_SEPARATOR .  $config['path'];
        }
        
        if (!empty($config['enabled'])) {
             $this->enable();
        }
        
        if (!file_exists(dirname($this->_path))) {
            mkdir(dirname($this->_path), 0755, true);
        }
        
        file_put_contents($this->_path, "");
    }
    
    public function log($data)
    {
        if (!$this->isEnabled()) {
            return false;
        }
        
        echo $data;
        return file_put_contents($this->_path, $data, FILE_APPEND);
    }
    
    public function enable()
    {
        $this->_enabled = true;
        
        return $this;
    }
    
    public function disable()
    {
        $this->_enabled = false;
        
        return $this;
    }
    
    public function isEnabled()
    {
        return $this->_enabled;
    }
}